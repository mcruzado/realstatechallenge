// eslint-disable-next-line import/prefer-default-export
export const getWords = (array) => {
  const frequency = {};
  array.forEach((elem) => {
    const words = typeof elem.palabrasItem !== 'object' ? JSON.parse(elem.palabrasItem.replace(/'/g, '"')) : elem.palabrasItem;
    Object.keys(words).forEach((k) => {
      frequency[k] = !frequency[k] ? parseInt(words[k], 0) : parseInt(words[k], 0) + frequency[k];
    });
  });
  return Object.keys(frequency).reduce((acc, k, i) => (
    [...acc, { id: i, word: k, frequency: frequency[k] }]
  ), []).sort((a, b) => b.frequency - a.frequency);
};

export const getItemFrequency = (array) => array.reduce((acc, elem) => {
  const indexOf = acc.findIndex((x) => x.item === elem.nameItem);
  if (indexOf === -1) {
    return [...acc, { id: elem.idItem, item: elem.nameItem, frequency: 1 }];
  }
  acc[indexOf].frequency = parseInt(acc[indexOf].frequency, 0) + 1;
  return [...acc];
}, []).sort((a, b) => b.frequency - a.frequency);
