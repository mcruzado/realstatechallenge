import Swal from 'sweetalert2';

// eslint-disable-next-line import/prefer-default-export
export const cornerAlert = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer);
    toast.addEventListener('mouseleave', Swal.resumeTimer);
  },
});

export const getErrorSwal = (msg) => Swal.fire({
  title: 'Error!',
  text: msg,
  icon: 'error',
});
