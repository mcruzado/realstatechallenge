import { combineReducers } from 'redux';
import { dataReducer } from './dataReducer';
import { entriesReducer } from './entriesReducer';

// eslint-disable-next-line import/prefer-default-export
export const reducers = combineReducers({
  data: dataReducer,
  entries: entriesReducer,
});
