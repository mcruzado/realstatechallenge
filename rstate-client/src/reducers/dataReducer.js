const defaultData = {
  contents: [],
  headers: [],
  words: [],
  items: [],
};

// eslint-disable-next-line import/prefer-default-export
export const dataReducer = (state = defaultData, action) => {
  if (action.type === 'SAVE_DATA') return action.dataInfo;
  if (action.type === 'CLEAR_DATA') return defaultData;
  return state;
};
