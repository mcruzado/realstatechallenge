const defaultEntries = [];

// eslint-disable-next-line import/prefer-default-export
export const entriesReducer = (state = defaultEntries, action) => {
  if (action.type === 'SAVE_ENTRIES') return action.entriesInfo;
  return state;
};
