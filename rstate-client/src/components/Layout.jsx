import React from 'react';
import FileReader from './FileReader';
import FileViewer from './FileViewer';
import Statistics from './Statistics';
import Navbar from './Navbar';
import { connect } from 'react-redux';

/** @jsx jsx */
import { css, jsx } from '@emotion/core';

const Layout = props => (
  <React.Fragment>
    <Navbar />
    <div className="columns" css={css`margin-top:65px`}>
      <div className="column is-narrow">
        <Statistics />
      </div>
      <div className="column" css={css`margin-top:20px !important;`}>
        <FileReader />
        {props.data.contents.length > 0 && (<FileViewer />)}
      </div>
    </div>
  </React.Fragment>
);

//Redux states/dispatchers
const mapStateToProps = (globalState) => ({
  data: globalState.data,
});

export default connect(mapStateToProps, null)(Layout);
