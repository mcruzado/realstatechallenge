import React from 'react';
import axios from 'axios';
import * as actions from '../actions';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';

import { cornerAlert, getErrorSwal } from '../tools/alerts';

/** @jsx jsx */
import { css, jsx } from "@emotion/core";

const FileReader = (props) => {

  //States
  const [loading, setLoading] = React.useState(false);
  const [file, setFile] = React.useState();
  const [filename, setFilename] = React.useState('');
  const [source, setSource] = React.useState('file')
  const [selectedDbName, setSelectedDbName] = React.useState(1);

  //Basic updaters
  const updateFile = (event) => {
    const file = event.currentTarget.files[0];
    if (file.name.endsWith('.csv')) {
      setFile(file);
    } else {
      getErrorSwal('Unsupported file type');
    }
  };

  const updateSelectedDbName = (event) => {
    setSelectedDbName(event.currentTarget.value);
  };

  const updateSource = (event) => {
    setSource(event.currentTarget.value);
  };

  //File uploader
  const sendFile = async (file) => {
    setLoading(true);
    let formData = new FormData();
    formData.append('file', file);
    try {
      const objPost = {
        headers: { 'Content-Type': 'multipart/form-data', },
      }
      const response = await axios.post('http://0.0.0.0:5000/api/upload', formData, objPost);
      setLoading(false);
      props.saveData(response.data);
      setFilename(file.name);
      cornerAlert.fire({
        icon: 'success',
        title: 'File opened successfully!'
      });
    } catch (e) {
      setLoading(false);
      if (props.data.contents.length === 0) {
        setFile(null);
        setFilename('');
      }
      getErrorSwal('CSV file cannot be processed. Please select a valid RealState Format CSV');
    }
  }

  //Statistic sets retriever
  const getEntriesFromDatabase = async () => {
    try {
      const response = await axios.get('http://0.0.0.0:5000/api/get');
      props.saveEntries(response.data);
    } catch (e) {
      getErrorSwal('Cannot retrieve entries from database. Please select a file');
    };
  };

  //Statisctic set loader
  const loadFromDatabase = async (id) => {
    try {
      const response = await axios.get(`http://0.0.0.0:5000/api/get/${id}`);
      Swal.fire({
        title: `${response.data.name}`,
        text: `Most repetitive words:${response.data.words}. \n Items with frequency:${response.data.items}`,
      });
    } catch (e) {
      getErrorSwal('There was an error retrieving selected statistics set');
    }
  };

  //Effect hooks
  React.useEffect(() => {
    if (file) sendFile(file);
  }, [file]);

  React.useEffect(() => {
    if (props.entries.length > 0) setSelectedDbName(props.entries[0].name)
  }, [props.entries]
  );

  React.useEffect(() => getEntriesFromDatabase(), []);

  //Render
  return (
    <section className="hero is mobile">
      <div className="container" css={css`margin-bottom:20px !important`}>
        <div className="columns">
          <div className="column">
            <div className="box" css={css`min-width:450px;min-height:150px`}>
              {loading && <div className="columns">
                <div className="column">
                  <div css={css`border:0px !important`} className="button is-large is-loading is-fullwidth"></div>
                </div>
              </div>}
              {!loading && <div>
                <div className="columns is-centered is-vcentered is-mobile">
                  <div className="column is-narrow has-text-centered">
                    <div className="control">
                      <label className="radio">
                        <input type="radio" value='file' onChange={updateSource} checked={source === 'file'} />
                        <small css={css`margin-left:5px !important;font-size:0.75em`}>Open file</small>
                      </label>
                      <label className="radio" disabled={props.entries.length === 0}>
                        <input type="radio" value='db'
                          onChange={updateSource} checked={source === 'db'}
                          disabled={props.entries.length === 0} />
                        <small css={css`margin-left:5px !important;font-size:0.75em`}>Stats history</small>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="columns is-centered is-vcentered is-mobile">
                  <div className="column is-narrow has-text-centered">
                    {source === 'file' && <div className="file has-name">
                      <label className="file-label">
                        <input className="file-input" type="file" name="resume" onChange={updateFile} />
                        <span className="file-cta">
                          <span className="file-icon">
                            <i className="fas fa-upload"></i>
                          </span>
                          <span className="file-label">Open CSV</span>
                        </span>
                        <span className="file-name">
                          <span css={css`font-size:0.8em;`}>
                            {filename ? filename : 'Choose a valid CSV file'}
                          </span>
                        </span>
                      </label>
                    </div>}
                    {source === 'db' && (
                      <div className="control">
                        <div className="columns">
                          <div className="column">
                            <div className="select">
                              <select onChange={updateSelectedDbName}>
                                {props.entries.map((e) => (
                                  <option value={e.name} key={e.id}>{e.name}</option>
                                ))}
                              </select>
                            </div>
                          </div>
                          <div className="column">
                            <button className="button" onClick={
                              () => loadFromDatabase(selectedDbName)
                            }>View</button>
                          </div>
                        </div>
                      </div>)}
                  </div></div>
              </div>}
            </div>
          </div>
        </div>
      </div>
    </section>
  )
};

//Redux states/dispatchers
const mapStateToProps = (globalState) => ({
  entries: globalState.entries,
  data: globalState.data
});

const mapDispatchToProps = {
  saveData: actions.saveData,
  clearData: actions.clearData,
  saveEntries: actions.saveEntries,
};

export default connect(mapStateToProps, mapDispatchToProps)(FileReader);