import React from 'react';
import axios from 'axios';
import CsvDownloader from 'react-csv-downloader';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

import * as utils from '../tools/filters';
import * as actions from '../actions';
import { cornerAlert, getErrorSwal } from '../tools/alerts';

import { AgGridReact } from 'ag-grid-react';

/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import { connect } from 'react-redux';

const FileViewer = (props) => {

  //States & Refs
  const [columns, setColumns] = React.useState([]);
  const [filtertoggle, setFiltertoggle] = React.useState(false);
  const [filterdata, setFilterdata] = React.useState([]);
  const [words, setWords] = React.useState([]);
  const [items, setItems] = React.useState([]);
  const [search, setSearch] = React.useState('');
  const rowRef = React.useRef();

  //CSV file functions
  const csvHeaders = React.useMemo(() => {
    return (props.data.columns).reduce((acc, v) =>
      [...acc, { id: v, displayName: v }]
      , [])
  }, [props.data.columns]);

  const getColumns = (colsArray) => colsArray.reduce((acc, cur) =>
    [...acc,
    {
      headerName: cur.charAt(0).toUpperCase() + cur.slice(1),
      field: cur,
      resizable: true,
      filter: true,
      sortable: true,
    }
    ], []);

  //Save to database function
  const saveRows = async () => {
    if (filterdata.length > 0) {
      const objPost = {
        headers: { 'Content-Type': 'application/json', },
      }
      try {
        const response = await axios.post('http://0.0.0.0:5000/api/save', JSON.stringify({
          words: getTopTenWords(words),
          items: getFreqItems(items),
        }), objPost);

        props.saveEntries(
          [...props.entries, {
            id: props.entries.length + 1,
            name: response.data.name,
          }])

        cornerAlert.fire({
          icon: 'success',
          title: 'Statistics saved successfully!'
        });
      } catch (e) {
        getErrorSwal('There was an error saving the statistics!');
      }
    }
  };

  //Table operations
  const filterRows = (event) => {
    event.preventDefault();
    rowRef.current.api.setQuickFilter(event.currentTarget.value);
    setSearch(event.currentTarget.value);
  };

  const saveGrids = () => {
    let fdata = [];
    rowRef.current.api.forEachNodeAfterFilterAndSort((rowNode, index) => {
      fdata.push(rowNode.data);
    });
    setFilterdata(fdata);
  };

  //In-live top words/item retrievers
  const getTopTenWords = (arr) => {
    return arr.slice(0, 9).reduce((acc, v) => acc + `, ${v.word}(${v.frequency})`, "").slice(1) || ' No results';
  };
  const getFreqItems = (arr) => {
    return arr.slice(0, 9).reduce((acc, v) => acc + `, ${v.item}(${v.frequency})`, "").slice(1) || ' No results';
  };

  //Effect hooks
  React.useEffect(() => {
    setWords(utils.getWords(filterdata));
    setItems(utils.getItemFrequency(filterdata));
  }, [filterdata]);

  React.useEffect(() => {
    rowRef.current.api.setQuickFilter(null);
    setSearch('');
    setFilterdata(props.data.contents);
  }, [props.data.contents]);

  React.useEffect(() => {
    setColumns(getColumns(props.data.columns))
  }, []);

  //render
  return (
    <div className="container-fluid" css={css`padding-right:20px !important`}>
      <div className="container">
        <div className="box is-size-7">
          <div className="columns" css={css`margin-bottom:2px !important`}>
            <div className="column">
              <div className="control" css={css`margin-bottom:0px !important`}>
                <input className="input" type="text" placeholder="Search by word" value={search} onChange={filterRows} />
              </div>
            </div>
            <div className="column is-3 has-text-right">
              <div className="control has-text-right">
                {filterdata.length > 0 &&
                  <CsvDownloader
                    filename="new_rsdata_filtered"
                    separator=";"
                    wrapColumnChar=""
                    columns={csvHeaders}
                    datas={filterdata}
                  >
                    <button className="button"><i css={css`margin-right:5px;`} className="fas fa-file-csv"></i>Save as CSV</button>
                  </CsvDownloader>}
              </div>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <div className="ag-theme-balham" css={css`height:380px !important;`}>

                <AgGridReact
                  columnDefs={columns}
                  rowData={props.data.contents}
                  pagination={true}
                  paginationAutoPageSize={true}
                  onSortChanged={() => saveGrids()}
                  onFilterChanged={() => saveGrids()}
                  ref={rowRef}
                >
                </AgGridReact>
              </div>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <span>
                <b>Rows:</b>
              </span>
              <span>
                {filterdata.length}
              </span>
            </div>
            <div className="column">
              <span>
                <b>Items:</b>
              </span>
              <span>
                {items.length}
              </span>
            </div>
            <div className="column">
              <span>
                <b>Words:</b>
              </span>
              <span>
                {words.length}
              </span>
            </div>
          </div>
          {filterdata.length > 0 && <div className="columns">
            <div className="column">
              <a onClick={() => setFiltertoggle(!filtertoggle)} css={css`font-size: 0.85rem`}>{!filtertoggle ? 'See Frequency Stats' : 'Hide'}</a>
            </div>
          </div>}

          {filtertoggle && filterdata.length > 0 && <div className="columns">
            <div className="column">
              <span>
                <b>MRW:</b>
              </span>
              <span>
                {getTopTenWords(words)}
              </span>
            </div>
            <div className="column">
              <span>
                <b>Items Frequency:</b>
              </span>
              <span>
                {` ${getFreqItems(items)}`}
              </span>
            </div>
          </div>}
          {filtertoggle && filterdata.length > 0 && <div className="columns">
            <div className="column has-text-right">
              <button className="button is-warning is-small" onClick={() => saveRows()}>
                <i className="fas fa-database" css={css`margin-right:5px !important`}></i>Save to Stats History</button>
            </div>
          </div>}
        </div>
      </div>
    </div>
  )
};

//Redux states/dispatchers
const mapStateToProps = (globalState) => ({
  data: globalState.data,
  entries: globalState.entries,
});

const mapDispatchToProps = {
  saveEntries: actions.saveEntries,
};

export default connect(mapStateToProps, mapDispatchToProps)(FileViewer);
