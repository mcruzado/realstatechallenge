/** @jsx jsx */
import { css, jsx } from "@emotion/core";

const Navbar = () => {
  return(
      <nav
      className="navbar is-white"
      css={css`
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.5) !important;
        position: fixed;
        top: 0px;
        width: 100% !important;
      `}
      role="navigation"
      aria-label="main navigation"
    >
      <div className="navbar-brand">
        <figure className="navbar-item" css={css`margin:5px`}>
          <img src={require("../img/predictiva.jpg")} width="112" alt="Predictiva" />
        </figure>

      </div>
      <div
        className="navbar-menu"
        css={css`
          z-index: 1 !important;
        `}
      >
        <div className="navbar-end">
          <div className="navbar-item">
            <span
              css={css`
                margin-left: 0px !important;
                font-size: 0.85em;
              `}
            >
              RealState Reader Concept (Mario Cruzado Ternero)
            </span>
          </div>
        </div>
      </div>
      </nav>
      )
};

export default Navbar;