import React from 'react';
import { connect } from 'react-redux';

/** @jsx jsx */
import { css, jsx } from "@emotion/core";

const Statistics = (props) => {

  //States
  const [topwords, setTopwords] = React.useState([]);
  const [toggleg, setToggleg] = React.useState(false);
  const [togglew, setTogglew] = React.useState(true);
  const [togglei, setTogglei] = React.useState(true);

  //Effect hooks
  React.useEffect(() => {
    setTopwords(props.data.words.slice(0, 9));
    setToggleg(true);
  }, [props.data.contents]);

  //Css models
  const menuList = css`color:rgb(120,120,120);margin-left:5px;margin-bottom:6px !important;font-size:1.32em;font-style:italic`;

  //Render
  return (
    <aside className="menu is-size-7" css={css`padding:15px !important;padding-left:25px !important;max-width:300px`}>
      { props.data.contents.length === 0 && 
      <div className="box">
      <p className="menu-label" css={css`font-size:1.5em;margin-bottom:50px !important;margin-left:5px;color:black`}>
        <span>Welcome buddy!</span>
      </p>
      <p css={menuList}>"This is a CSV reader prototype. 
      You can open a CSV file and, if it's compatible with RState standard format, 
      you will see all his content and static/in-live statistics.</p>
      <br />
      <p css={menuList}>You can find a valid file to start in resources folder."</p>
      <br />
      <p css={menuList}>Enjoy it!</p>
      </div>
      }
      { props.data.contents.length > 0 && <div>
      <p className="menu-label" css={css`font-size:1.4em`}>
        <span>Statistics (From Origin)</span>
      </p>
      <p className="menu-label" css={css`font-size:1.2em`}>
        <a onClick={() => setToggleg(!toggleg)}>Global Stats</a>
      </p>
      {toggleg && <ul className="menu-list">
        <li css={menuList}><span className="has-text-weight-bold">Rows</span>: {props.data.contents.length}</li>
        <li css={menuList}><span className="has-text-weight-bold">Words</span>: {props.data.words.length}</li>
        <li css={menuList}><span className="has-text-weight-bold">Items</span>: {props.data.items.length}</li>
      </ul>}
      <p className="menu-label" css={css`font-size:1.2em`}>
        <a onClick={() => setTogglew(!togglew)}> Most Repetitive Words</a>
      </p>
      {togglew && <ul className="menu-list">
        {topwords.map((w) => (
          <li css={menuList} key={`w${w.id}`}><span className="has-text-weight-bold">{w.word}</span>: {w.frequency}</li>
        ))}
      </ul>}
      <p className="menu-label" css={css`font-size:1.2em`}>
        <a onClick={() => setTogglei(!togglei)}> Items Frequency</a>
      </p>
      {togglei && <ul className="menu-list">
        {props.data.items.map((i) => (
          <li css={menuList} key={`i${i['id']}`}><span className="has-text-weight-bold">{i['item']}</span>: {i['frequency']}</li>
        ))}
      </ul>}
        </div> }
    </aside>
  )
};

//Redux states/dispatchers
const mapStateToProps = (globalState) => ({
  data: globalState.data,
});

export default connect(mapStateToProps, null)(Statistics);