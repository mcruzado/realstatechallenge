import React from 'react';
import './App.css';
import Layout from './components/Layout';
import 'bulma/css/bulma.css';

const App = () => {
  return (
  <React.Fragment>
    <Layout></Layout>
  </React.Fragment>
  );
}

export default App;
