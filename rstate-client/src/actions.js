// eslint-disable-next-line import/prefer-default-export
export const saveData = (dataInfo) => ({
  type: 'SAVE_DATA',
  dataInfo,
});

export const clearData = () => ({
  type: 'CLEAR_DATA',
});

export const saveEntries = (entriesInfo) => ({
  type: 'SAVE_ENTRIES',
  entriesInfo,
});
