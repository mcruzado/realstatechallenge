from database.config import db

class RsData(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(90), nullable=False)
  words = db.Column(db.Text, nullable=True)
  items = db.Column(db.Text, nullable=True)

  def __init__(self, name, words, items): 
    self.name = name
    self.words = words
    self.items = items