from io import StringIO
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from models.rsdata import RsData
from schemas.rsdata import RsDataSchema, ma
from flask_cors import CORS
from ast import literal_eval
from datetime import datetime

from database.config import db

import csv, json
import requests
import sqlite3
import os

app = Flask(__name__)

based = f'{os.path.abspath(os.path.dirname(__file__))}/database'

app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{os.path.join(based, "rsdb.sqlite")}'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'

# DB Initialization

with app.app_context():
  db.init_app(app)
  ma.init_app(app)
  db.create_all()
  db.session.commit()


# Managing CORS-BLOCK Policy

CORS(app)

# Csv File Processings

def csv_decode(file):
  return StringIO(file.stream.read().decode("utf-8-sig"), newline=None)
  
def csv_to_dict(file):
  return csv.DictReader(csv_decode(file), delimiter=";")

def valid_csv(headers):
  valid_headers = ['identificacionSubItem','palabrasItem','nameItem','idItem',
  'accuracyItem','validationItem','campaignCode','file']
  return True if valid_headers == headers else False

def valid_mimetype(file): 
  return True if file.content_type == 'text/csv' else False

def remove_duplicates(data):
  return [ obj for n, obj in enumerate(data) if obj not in data[n + 1:]]

def remove_invalid(data):
  return [ obj for obj in data if str(obj['palabrasItem']).strip() != '{}' ]

# Csv Stats collector

def count_words(data):
  frequency = dict()
  for row in data:
    for k, v in literal_eval(row['palabrasItem']).items():
      frequency[k] = (v if k not in frequency else v + frequency[k])
  freq_list = sorted([ 
    { 'id': i, 'word': k, 'frequency': v } 
    for i, (k, v) in enumerate(frequency.items()) ], 
    key=lambda x:x['frequency'], reverse=True)
  return freq_list

def count_items(data):
  frequency = dict()
  for row in data:
    frequency[row['nameItem']] = 1 if row['nameItem'] not in frequency else frequency[row['nameItem']] + 1
  freq_list = [
    { 'id': i, 'item': k, 'frequency': v }
    for i, (k, v) in enumerate(frequency.items()) ]
  return freq_list

# Routes

@app.route('/api/', methods=['GET'])
def get_salute():
  return {'message': 'Hello. Welcome to RState API'}

@app.route('/api/upload', methods=['GET', 'POST'])
def upload_csv():
  if 'file' not in request.files:
    return { 'message': 'File not provided'}, 400

  fileUploaded = request.files['file']

  if not valid_mimetype(fileUploaded):
    return { 'message': 'Unsupported file type'}, 400

  data_dict = csv_to_dict(fileUploaded)

  if not valid_csv(data_dict.fieldnames): 
    return { 'message': 'Invalid csv headers'}, 400

  contents = remove_duplicates(remove_invalid([row for row in data_dict]))

  return {
    'contents': contents,
    'words': count_words(contents),
    'items': count_items(contents),
    'columns': data_dict.fieldnames
  }

# Db endpoints

rsdata_schema = RsDataSchema()
rsdatas_schema = RsDataSchema(many=True)

@app.route('/api/save', methods=['GET', 'POST'])
def save_to_database():
  if not request.is_json:
    return { 'message': 'Bad Request'}, 400

  data = request.get_json()

  generated_name = f'datasaved-{datetime.utcnow().strftime("%Y-%M-%d_%H%M%S")}'
  content_to_save = RsData(generated_name, str(data['words']), str(data['items']))

  db.session.add(content_to_save)
  db.session.commit()

  return { 'message': 'Saved Successfully', 'name': generated_name }

@app.route('/api/get', methods=['GET'])
def get_rsdatas():
  datas = RsData.query.with_entities(RsData.id, RsData.name).all()
  result = rsdatas_schema.dump(datas)
  
  return jsonify(result)

@app.route('/api/get/<name>', methods=['GET'])
def get_concrete_data(name): 
  data = RsData.query.filter_by(name=str(name)).first()
  result = rsdata_schema.jsonify(data)

  return result

if __name__ == '__main__':
  app.run(debug=True, port=5000)

  