from flask_marshmallow import Marshmallow

ma = Marshmallow()

class RsDataSchema(ma.Schema):
  class Meta:
    fields = ('id', 'name', 'words', 'items')