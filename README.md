# RealStateChallenge

Predictiva.io code challenge

## Challenge description

Predictiva is opening a new business around the Real State sector.
One of our potential clients is an international Real State company. They need to supervise their sales process and they want to track how good are their sale agents performing. They would like to know the important words used by their agents during the conversations with their customers, and other metrics...
Currently they have done some progress collecting the conversations with the most relevant phrases in excel. We will call these relevant phrases, items. Agents are collecting and send these items in an excel sheet every day. The supervisors collect these excel files, they merge them all together in a big excel one containing all the data collected from the agents, for that specific day (you will find an example on this merged file containing in this repository).
So ... we need to build an app to make this process more profitable since they spend a lot of time doing things manually and here is where from Predictiva we are providing a killing solution for them and others in the same sector. However ... we are running out of resources and we are considering to outsource this development ... and ... you know? ... looks like you are our best candidate!
Yes, you heard right! You are going to be the person in charge of giving a full solution to this problem, from the concept to the implementation, and this client is so important that we expect the best of your skills to demonstrate that you are the guy we need!
We understand that you will decide the best technologies to solve this problem. However, our staff and solutions are built around Angular, Python Flask and SQL Alchemy technologies, so have in mind that we will need at some point to receive the handover of your work to maintain it and improve it.

## Requirements

    1) Processing of CSV file to feed our real state application.
    2) Filter out invalid as well as repeated items (consider two items are the same if all of the fields are exactly the same).
    3) Processes the good items and visualise them in the application.
    4) Display the frequency of the items.
    5) Display the ten most repetitive words.
    6) Allow the user to perform some operations on the items, such as: filtering, sorting, etc.
    7) Give the posibility to the user to store the results.

    Extra:
    1) Synchronise the visualisation while applying filtering.
    2) What you see is what you'll get.

## Extra-Goals achieved

    1) Synchronise the visualization while applying filtering.
    2) What you see is what you'll get (I think it means that if you save the records displayed, you will get it saved in the CSV file that you export)
    ++ Local deployment dockerizing the application to launch it on any computer with pre-installed docker
    ++ Create a complete Full Stack Application (Frontend-Backend-DB)

## Technologies

    - Front-End:

      · Node: 12.16.2
      · React: ^16.13.1
      · Redux: ^4.0.5
      · Emotion (CSS in JS): ^10.0.28
      · Bulma (CSS Flexbox): ^0.8.2
      · Ag-Grid (Dynamic tables for JavaScript): ^23.0.3
      · React Csv Downloader: ^1.6.0

    - Back-End:

      · Python: 3.7
      · Flask: 1.1.2
      · SQLAlchemy: 1.3.16
      · Flask-Marshmallow (Db Schemas):
    
    - Database:

      · SQLite3 on Flask-SQLAlchemy (Stored inside the API)

    - Deployment:

      · Docker

## Unfilled Desires (Apologize for timing lack)

    - Basic testing for API and Frontend
    - I would have liked to implement POSTGRESQL instead SQLITE to save json lists with complete files

## Technical Requirements to run the application:

  - Docker: https://docs.docker.com/get-docker/ 
  - (If you have only installed docker for command line and you have not installed docker for desktop, you will need also install 'docker-compose' and set it up)

## Repository

  - git clone https://gitlab.com/mcruzado/realstatechallenge.git

## Run

    - Option 1:   
        make run
    - Option 2:   
        docker-compose up -d
        
## View on web browser
    
    - Navigate http://localhost:3000 (After run)